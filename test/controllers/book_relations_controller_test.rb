require 'test_helper'

class BookRelationsControllerTest < ActionController::TestCase
  setup do
    @book_relation = book_relations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:book_relations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create book_relation" do
    assert_difference('BookRelation.count') do
      post :create, book_relation: { book_id: @book_relation.book_id, owned: @book_relation.owned, read: @book_relation.read, user_id: @book_relation.user_id, wanted: @book_relation.wanted }
    end

    assert_redirected_to book_relation_path(assigns(:book_relation))
  end

  test "should show book_relation" do
    get :show, id: @book_relation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @book_relation
    assert_response :success
  end

  test "should update book_relation" do
    patch :update, id: @book_relation, book_relation: { book_id: @book_relation.book_id, owned: @book_relation.owned, read: @book_relation.read, user_id: @book_relation.user_id, wanted: @book_relation.wanted }
    assert_redirected_to book_relation_path(assigns(:book_relation))
  end

  test "should destroy book_relation" do
    assert_difference('BookRelation.count', -1) do
      delete :destroy, id: @book_relation
    end

    assert_redirected_to book_relations_path
  end
end
