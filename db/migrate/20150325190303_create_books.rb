class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author_first_name
      t.string :author_last_name
      t.string :publisher
      t.string :isbn10
      t.string :isbn13
      t.string :book_length

      t.timestamps
    end
  end
end
