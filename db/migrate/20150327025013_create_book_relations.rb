class CreateBookRelations < ActiveRecord::Migration
  def change
    create_table :book_relations do |t|
      t.integer :user_id
      t.integer :book_id
      t.boolean :owned
      t.boolean :wanted
      t.boolean :read

      t.timestamps
    end
  end
end
