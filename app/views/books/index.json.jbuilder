json.array!(@books) do |book|
  json.extract! book, :id, :title, :author_first_name, :author_last_name, :publisher, :isbn10, :isbn13, :book_length
  json.url book_url(book, format: :json)
end
