json.extract! @book, :id, :title, :author_first_name, :author_last_name, :publisher, :isbn10, :isbn13, :book_length, :created_at, :updated_at
