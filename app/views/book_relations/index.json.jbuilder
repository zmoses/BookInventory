json.array!(@book_relations) do |book_relation|
  json.extract! book_relation, :id, :user_id, :book_id, :owned, :wanted, :read
  json.url book_relation_url(book_relation, format: :json)
end
