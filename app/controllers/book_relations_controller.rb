class BookRelationsController < ApplicationController
  before_action :set_book_relation, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @book_relations = BookRelation.all
    respond_with(@book_relations)
  end

  def show
    respond_with(@book_relation)
  end

  def new
    @book_relation = BookRelation.new
    respond_with(@book_relation)
  end

  def edit
  end

  def create
    @book_relation = BookRelation.new(book_relation_params)
    @book_relation.save
    respond_with(@book_relation)
  end

  def update
    @book_relation.update(book_relation_params)
    respond_with(@book_relation)
  end

  def destroy
    @book_relation.destroy
    respond_with(@book_relation)
  end

  private
    def set_book_relation
      @book_relation = BookRelation.find(params[:id])
    end

    def book_relation_params
      params.require(:book_relation).permit(:user_id, :book_id, :owned, :wanted, :read)
    end
end
